import sys
from os.path import dirname, realpath
from ami_data_parser import *


test_data_path = dirname(realpath(__file__)) + '/../tests/test-data/'

for t in ['a', 'b']: # 'c', 'd', 'e', 'f', 'g', 'invalid']:
    filename = test_data_path + 'type_%s.csv' % (t,)
    print(filename)
    with open(filename) as f:
      (df, meta) = parse(f)
    print(df.shape)
    print('First value:', df.iloc[0])
    print('Last value:', df.iloc[len(df.index) - 1])
