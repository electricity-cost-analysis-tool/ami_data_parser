import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from ami_data_parser import AmiDataParserException


def parse(data):
  ''' Load data in our format: A datetime and one or value columns. '''
  try:
    df = pd.read_csv(data,)
    df['datetime'] = pd.to_datetime(df['datetime'], errors='raise', infer_datetime_format=True)
    if len(df.columns) < 2:
      AmiDataParserException('No data columns found')
    return (df, {})
  except Exception as e:
    raise AmiDataParserException(e)

