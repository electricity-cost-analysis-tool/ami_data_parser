import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from ami_data_parser import AmiDataParserException


def parse(data):
  ''' Load a interval data file and return as a panda.DataFrame, with first columns <datetime, value>
  For Type 1 interval data file format we need to:
    - skip the first 1 rows (skiprows=1)
    - use columns 3,5,6,7,8 ... 53 (usecols)
    - only keep 1+48 colums after the first (usecols, and reset columns to zero indexed with columns setter)
    - reshape, re-date
  This routine does not ensures there is a year or any other number worth of rows.
  '''
  try:
    df = pd.read_csv(
      data,
      header=None,
      skiprows=1,
      skip_blank_lines=True,
      usecols=np.insert(np.arange(5, 53), 0, 3),
      converters={3: lambda d: datetime.strptime(str(d), "%d/%b/%Y") if d else None}
    )
    start_datetime = df.iloc[0,0]
    df = pd.DataFrame(df.iloc[:,1:].values.flatten(), columns=['value'])
    df['datetime'] = [start_datetime + timedelta(minutes=i*30) for i in range(len(df))]
    return (df, _make_meta(df))
  except Exception as e:
    raise AmiDataParserException(e)


def _make_meta(df):
  return {
    'interval': 30
  }
