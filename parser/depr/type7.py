import numpy as np
import pandas as pd
from datetime import datetime
from ami_data_parser import AmiDataParserException


def parse(data):
    ''' Load a interval data file and return as a panda.DataFrame, with first column a datatime and
    subsequent 48 columns the interval readings as floats. For Type E interval data file format we need to:
      - This type has vertical breakdown of consumption.
      - use columns 6,7 & 8 (usecols)
      - First column contains 48 date values for each day. Therefore 366*48 = 17568 rows.
      - Second Column has the start time value for that interval.
      - Third column has the actual consumption value.
      - Parse dates by combining the date and time into 'timestamp' so that it can be reshaped.
      - set the data type for the first col to datetime and parse it into a datetime.
      - set the index as specified headers and unstack it so that each day contains 48 columns.
      - Add the index into the DF to give it shape X,49
      - additional check to ascertain whether the first column of the first row indeed has a valid datetime object
    This routine does not ensure there is a year or any other number worth of rows.
    '''
    headers = ['StartDate', 'StartTime', 'ProfileReadValue']
    try:
        df = pd.read_csv(
            data,
            names=headers,
            parse_dates={'timestamp': ['StartDate', 'StartTime']},
            skip_blank_lines=True,
            usecols=[1, 2, 7],
            converters={1: lambda d: datetime.strptime(str(d), "%d/%m/%Y") if d else None}
        )
        df = df.set_index([df.timestamp.dt.date, df.timestamp.dt.time]).ProfileReadValue.rename_axis([None] * 2).unstack()
        df.reset_index(level=0, inplace=True)

        df.iloc[0][0].dayofyear
        df.columns = range(0, len(df.columns))
        return df

    except Exception as e:
        raise AmiDataParserException(e)
