import numpy as np
import pandas as pd
from datetime import datetime
from ami_data_parser import AmiDataParserException


def parse(data):
    ''' Load a interval data file and return as a panda.DataFrame, with first column a datatime and
    subsequent 48 columns the interval readings as floats. For Type D interval data file format we need to:
      - skip the first 1 rows (skiprows=1)
      - use columns 3,6,7,8...54 (usecols)
      - only keep 1+48 colums after the first (usecols, and reset columns to zero indexed with columns setter)
      - only keep max of 366 rows starting from the first kept row (numpy slicing)
      - set the data type for the first col to date and parse it into a datetime, and set the rest to floats.
      - additional check to ascertain whether the first column of the first row indeed has a valid datetime object
    This routine does not ensure there is a year or any other number worth of rows.
    '''
    try:
        df = pd.read_csv(
            data,
            header=None,
            skiprows=1,
            skip_blank_lines=True,
            usecols=np.insert(np.arange(6, 54), 0, 3),
            converters={3: lambda d: datetime.strptime(str(d), "%d/%m/%y") if d else None}
        )
        df.columns = range(0, len(df.columns))
        df.iloc[0][0].dayofyear
        return df
    except Exception as e:
        raise AmiDataParserException(e)
