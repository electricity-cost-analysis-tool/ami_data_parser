import numpy as np
import pandas as pd
from datetime import datetime
from ami_data_parser import AmiDataParserException


def parse(data):
    ''' Load a interval data file and return as a panda.DataFrame, with first column a datatime and
    subsequent 48 columns the interval readings as floats. For Type C interval data file format we need to:
      - skip the first 4 rows (skiprows=4)
      - use columns 0 to 49 (usecols)
      - only keep 1+48 colums after the first (usecols, and reset columns to zero indexed with columns setter)
      - set the data type for the first col to date and parse it into a datetime, and set the rest to floats.
      - additional check to ascertain whether the first column of the first row indeed has a valid datetime object
    This routine does not ensure there is a year or any other number worth of rows.
    '''
    try:
        df = pd.read_csv(
            data,
            header=None,
            skiprows=4,
            skip_blank_lines=True,
            usecols=np.arange(0, 49),
            converters={0: lambda d: datetime.strptime(str(int(d)), "%Y%m%d") if d else None}
        )
        df.columns = range(0, len(df.columns))
        df.iloc[0][0].dayofyear
        return df
    except Exception as e:
        raise AmiDataParserException(e)
