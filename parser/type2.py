import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from ami_data_parser import AmiDataParserException


def parse(data):
  ''' Load a interval data file and return as a panda.DataFrame, with first columns <datetime, value>
  For Type 1 interval data file format we need to:
    - skip the first 2 rows (skiprows=2)
    - use columns 1 to 50 (usecols)
    - only keep 1+48 columns after the first (usecols, and reset columns to zero indexed with columns setter)
    - reshape, re-date
  This routine does not ensure there is a year or any other number worth of rows.
  @todo better fix for trailing incomplete rows. 
  '''
  try:
    df = pd.read_csv(
      data,
      header=None,
      skiprows=2,
      skip_blank_lines=True,
      usecols=np.arange(1, 50),
      converters={1: lambda d: datetime.strptime(str(int(d)), "%Y%m%d") if d else None}
    )
    start_datetime = df.iloc[0,0]
    df = df[~df[1].isnull()] # Hack to filter trailing incomplete rows. 
    df = pd.DataFrame(df.iloc[:,1:].values.flatten(), columns=['value'])
    df['datetime'] = [start_datetime + timedelta(minutes=i*30) for i in range(len(df))]    
    return (df, _make_meta(df))
  except Exception as e:
    raise AmiDataParserException(e)
    
    
def _make_meta(df):
  return {
    'interval': 30
  }
