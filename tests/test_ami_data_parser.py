from os.path import dirname, realpath
from copy import deepcopy
import timeit
import pandas
from datetime import *
import unittest
from unittest import TestCase
from ami_data_parser import parse


test_data_path = dirname(realpath(__file__)) + '/test-data/'


class TestAmiDataParser(TestCase):
    ''' Basic tests for AMI data file parsers. '''

    def test_all_parse(self):
        for p in ['a', 'b']: # 'c', 'd', 'e', 'f', 'g']:
            filename = test_data_path + 'type_%s.csv' % (p,)
            (df, meta) = parse(filename)
            self.assertEqual(df.shape[1], 2)
            self.assertTrue(df.shape[0] > 360*48)
    
    @unittest.skip('Not implemented')
    def test_align(self):
        (df, meta) = parse(test_data_path + 'type_a.csv')
        df = align_year(df)
        f_row = df.iloc[0]
        l_row = df.iloc[len(df.index) - 1]
        f_day = f_row[0]
        l_day = l_row[0]
        self.assertEqual(len(f_row), 49)
        self.assertEqual(type(f_day), pandas.tslib.Timestamp)
        self.assertEqual(f_day.strftime('%A'), 'Monday')

    def test_no_data(self):
        with self.assertRaises(ValueError) as e:
            (df, meta) = parse('dne')

    def test_invalid_data(self):
        with self.assertRaises(ValueError) as e:
            (df, meta) = parse([1, 2, 3])
        with self.assertRaises(ValueError) as e:
            filename = test_data_path + 'type_invalid.csv'
            (df, meta) = parse(filename)

    # @unittest.skipIf(True, reason="time")
    def test_time(self):
        for p in ['a', 'b']: # 'c', 'd', 'e', 'f', 'g']:
            filename = test_data_path + 'type_%s.csv' % (p,)
            t = timeit.timeit(lambda: parse(filename), number=1)
            print('%s: %f' % (p, t))
            self.assertTrue(t < 6)
