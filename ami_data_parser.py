'''
Front end to collection of parsers for different formats of AMI data file known to us.
A parser is currently just a module with parse() function that take a string, data, or readable 
(has .read() method) and returns a pandas.DataFrame with a given format. See README.md for spec.

Parsers must raise AmiDataParserException if the file cannot be parsed for any reason.
'''
import os
import sys
import importlib
import pandas as pd
import numpy as np
import logging
from os.path import dirname, basename
from glob import glob
from datetime import *
from ami_data_parser import AmiDataParserException


def parse(data):
    ''' Try parse `data` and return result. `data` can have mixed form - generally anything accepted
    by pandas.read_csv() (filename, string, readable stream). If a file is parsed successfully
    returns a pandas.Dataframe, otherwise raise a ValueError.

    The returned DataFrame will have 1 row for each day. The first column is a datetime. Subsequent
    columns are interval reading for the entire day. Example, 48 readings for 30m intervals.
    '''
    for parser in parsers():
        try:
            logging.info('Try %s' % (parser,))
            (df, meta) = parser.parse(data)
            meta = _make_meta(df, meta)
            logging.info('Parser %s OK' % (parser,))
            return (df, meta)
        except AmiDataParserException as e:
            logging.debug('Parser %s failed [%s]' % (parser, e))
            try:
              data.seek(0)
            except AttributeError as e:
              pass
    raise ValueError('Data could not parsed by any known parser')


def _make_meta(df, meta):
  ''' Add some meta data for conv. '''
  _meta = {
    'start': df['datetime'].iloc[0].to_pydatetime(),
    'end': df['datetime'].iloc[-1].to_pydatetime(),
    'length': len(df)
  }
  meta.update(_meta)
  return meta


def get_latest_whole_year(df):
    ''' Align and clip the dataframe `df` as returned by parse() to the latest whole one year starting
    first Monday of January. Note rows occuring before that Monday may be added to the end of the 
    rage so that a whole year can be provided. Dates are not updated.
    '''
    pass


def parsers():
    ''' Load and yeild each parser module. '''
    parser_dir = os.path.dirname(__file__) + '/parser/'
    parsers = list(map(lambda f: basename(f), glob(parser_dir + '*.py')))
    parsers = list(filter(lambda f: not f.startswith('__'), parsers))
    for parser in sorted(parsers):
        modname = 'ami_data_parser.parser.' + parser.replace('.py', '')
        yield importlib.import_module(modname)

