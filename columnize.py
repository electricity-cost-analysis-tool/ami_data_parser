import pandas as pd
import numpy as np
from datetime import datetime


# All columns in DataFrame returned by columnized
columns=['value', 'datetime', 'year', 'mon', 'yday', 'mday', 'wday', 'hour', 'min', 'sec', 'qtr', 'ssn']
# Entries in columns created by columnized are all ints (usually starting at 0). These are common
# names for those values.
labels = {
  'ssn': ['Summer', 'Autumn', 'Winter', 'Spring'],
  'qtr': ['Q1', 'Q2', 'Q3', 'Q4'],
  'wday': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  'mon': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
}

    
def columnize(data):
  '''
  Explode a timeseries DataFrame with a datetime column into broken down time parts. Based off UNIX 
  struct tm, plus Quarter (qtr) and Season (ssn).
  
      struct tm {
        int tm_sec;         /* seconds */
        int tm_min;         /* minutes */
        int tm_hour;        /* hours */
        int tm_mday;        /* day of the month */
        int tm_mon;         /* month */
        int tm_year;        /* year */
        int tm_wday;        /* day of the week */
        int tm_yday;        /* day in the year */
        int tm_isdst;       /* daylight saving time */
      };
      
  Extremely slow:
  
    df.apply(lambda v: pd.concat((v, pd.Series(datetime_to_tm(v['datetime'])))), axis=1)
  '''
  rows = []
  values = data['value'].values
  datetimes = list(data['datetime'])
  for i, dt in enumerate(datetimes):
    line = datetime_to_tm(dt)
    line['datetime'] = dt
    line['value'] = values[i]
    rows.append(line)
  frame = pd.DataFrame(rows, columns=columns[:-2])
  frame['qtr'] = frame['mon'].apply(qtr)
  frame['ssn'] = frame['mon'].apply(ssn)
  return frame
    

def datetime_to_tm(dt):
  ''' datetime.timetuple() is not a namedtuple so has no _asdict(). '''
  tm = dict(zip(['year', 'mon', 'mday', 'hour', 'min', 'sec', 'wday', 'yday', 'isdst'], dt.timetuple()))
  del tm['isdst']
  return tm


def qtr(v):
  ''' Return number between [0-3] '''
  return int((v+2)/(3))-1
  

def ssn(v):
  ''' Return number between [0-3] '''
  return int(((v%12)+3)/3)-1


def dayname(v):
  return days[v]

