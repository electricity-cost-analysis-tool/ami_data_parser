# AMI Data Parser
This Python package contains parsers for parsing (Australian) AMI (Advance Metering Infrastructure) data readings, generally provided in the form of a CSV or similar formatted file. The main public interface to this package is `parse(filepath_or_buffer data): tuple(DataFrame data, dict metadata)`. This function loads and attempts to parse the AMI data and returns the data as a Pandas.DataFrame if it can do so. Parsers for known file formats are in the [parser/](parser/) dir. See below for parser interface.

# Parsers
Parsers are delegated to be the `parse()` function to do the parsing. Parsers implement the following two reentrant functions:

    parse(IOBase data, **kwargs): (pandas.DataFrame data, dict metadata)
    finger(str first-few-lines, **kwargs): bool
  
Arbitrary, parser specific arguments to `parse()` may be defined on a parser specific basis. `finger()` is currently optional but will be made mandatory in future.

## Data Series Table Output Format
time series data shall be represented as a DataFrame with a number of named columns, and a row for each reading. Rows are indexed by integer offset.

    datetime: A datetime of the given reading.
    \<value\>+: readings.

The `datetime` column is mandatory. One ore more value columns must be present. The following common names should be used if the semantics described matches the data:

    value: Only one data series is present and it's semantics are unknown (could be import export or something else).
    export: Standard export (from grid to user) reading from a meter.
    import: Standard import (from user to grid) reading from a meter.
    powerfactor: Standard meter powerfactor reading as a unitless.

  1. Values of the `value`, `export`, `import` entries shall be decimal or whole numbers with assumed units of `kW`. `powerfactor` entries shall be decimal or whole unitless numbers between [0,1].
  2. A module should attempt to provide data series with the above names if data is available in the meter file with the above semantics, but not in the format described. For example, if powerfactor is given as a percentage it should be converted to a value between [0,1]; if value is known to be listed in Watts it should be converted to kW; if powerfactor is not listed but can be derived it should be derived.
  3. All columns must have the same length. If the parsing module needs to down or up sample some rows to make this happen it should do so. Alternatively the weird columns can be omitted.
  4. The datetime values should be exactly that listed in the origin data file. For example if the datetime does not line up with some interval increment it should be left that way. A secondary processing function will be available to align entries in some specified way.
  5. empty/nil/null/unreadable values shall be replaced with `nan`. A secondary processing function will be made available to optionally de-nullify entries in some specified way.

**Meta data Output Format**<br/>
Meta data optionally be returned as a dictionary. The following keys have the following meaning:

  - nmi: national meter identier
  - interval: interval of data in minutes.
  - description: arbitrary human readable description of stuff.  
  - provider: a dict containing:
    - name: common name
    - id: some identifier
  - series: a dict. Each key describes the so named column of the table returned by the parser. keys shall be the name of the column described.
    - unit: ...
    - description: ...
  - start: datetime
  - end: datetime

## Known Parsers
This module currently has parsers for a few file types. See code docs, and [AMI Data Parser File Types Research](docs/smart-meter-data-file-formats.md).
