# Formats for .csv files to be parsed by ami-data-parser.py
The <a href='#t1'>table below</a> shows a mapping between *Victorian* retailer *residential* tariffs and the smart meter data file format that they use. There are over 20 retailers in Victoria, but only 7 distinct smart meter data file formats (that we are aware of). At this stage other Australian states have not been considered explicitly, although many retailers have operations in all NEM states.

No concrete information could be found about the smart meter data file formats used in business tariffs. However evidence suggests they are different. Business meter data file formats are not currently supported.

The file formats in the table below were derived indirectly from two sources; [Victorian Energy Compare][1], and [this][2] AEMC document which is a proposal letter written by the *Department of State Development, Business and Innovation*, for a tool called *"My Power Planner"* which was the predecessor for Victorian Energy Compare.


<a name='t1'></a>

| ami_data_parser Name | Vic Energy Compare Format Name                | AEMC Format Name   | Notes                             |
|----------------------|-----------------------------------------------|--------------------|-----------------------------------|
| Type 1               | Jemena, UE                                    | Format 1, Format 2 | Date Format different in Source 2 |
| Type 2               | NA                                            | NA                 | File provided by Client           |
| Type 3               | Alinta, Dodo, Neighbourhood, Simply, Momentum | Format 3           | Different blocks in Source 2      |
| Type 4               | AGL, Diamond, Origin                          | Format 4           | Same (Vertical Breakdown)         |
| Type 5               | Lumo                                          | Format 5           | Same                              |
| Type 6               | Blue, ERM, Red, AusNet                        | Format 6           | Date Format different in Source 2 |
| Type 7               | EA, CitiPower, Powercor                       | Format 7           | Same (Vertical Breakdown)         |


## Screenshots of retail formats
Source of these screen is the [AEMC report][2].

#### Type 1:
![Type 1](img/ami-file-type-A.JPG)

#### Type 2:
![Type 2](img/ami-file-type-B.JPG)

#### Type 3:
![Type 3](img/ami-file-type-C.JPG)

#### Type 4:
![Type 4](img/ami-file-type-D.JPG)

#### Type 5:
![Type 5](img/ami-file-type-E.JPG)

#### Type 6:
![Type 6](img/ami-file-type-F.JPG)

#### Type 7:
![Type 7](img/ami-file-type-G.JPG)

[1]: https://www.victorianenergysaver.vic.gov.au/victorian-energy-compare/compare-offers-with-victorian-energy-compare/how-to-download-an-electricity-usage-file-from-your-provider
[2]: http://www.aemc.gov.au/getattachment/da7875c6-1915-438f-bc3b-daea62402352/Victorian-Department-of-State-Development-Business.aspx
[3]: https://compare.switchon.vic.gov.au/
